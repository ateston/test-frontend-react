import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/navbar.component";
import CarsList from "./components/cars-list.component";
import CreateCar from "./components/create-car.component";

function App() {
  return (
    <Router>
      <div className="container">
        <Navbar />
        <br />
        <Route path="/" component={CarsList} />
        <Route path="/create" component={CreateCar} />
      </div>
    </Router>
  );
}

export default App;
