import React, { Component } from 'react';
import axios from 'axios';

export default class CreateCar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      brand: '',
      model: '',
      year: 1900,
      color: '',
      vin: ''
    }

    this.onChangeBrand = this.onChangeBrand.bind(this);
    this.onChangeModel = this.onChangeModel.bind(this);
    this.onChangeYear = this.onChangeYear.bind(this);
    this.onChangeColor = this.onChangeColor.bind(this);
    this.onChangeVin = this.onChangeVin.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

  }

  onChangeBrand(e) {
    this.setState({
      brand: e.target.value
    });
  }

  onChangeModel(e) {
    this.setState({
      model: e.target.value
    });
  }

  onChangeYear(e) {
    this.setState({
      year: e.target.value
    });
  }

  onChangeColor(e) {
    this.setState({
      color: e.target.value
    });
  }

  onChangeVin(e) {
    this.setState({
      vin: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    const newCar = {
      brand: this.state.brand,
      model: this.state.model,
      year: this.state.year,
      color: this.state.color,
      vin: this.state.vin
    };

    console.log(newCar);
    axios.post('http://localhost:5000/cars/add', newCar)
      .then(res => console.log(res.data));

    window.location = '/';
  }

  render() {
    return (
      <div>
        <h3>Add new Car</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Brand: </label>
            <input type="text"
              required
              className="form-control"
              value={this.state.brand}
              onChange={this.onChangeBrand}
            />
          </div>
          <div className="form-group">
            <label>Model: </label>
            <input type="text"
              required
              className="form-control"
              value={this.state.model}
              onChange={this.onChangeModel}
            />
          </div>
          <div className="form-group">
            <label>Year: </label>
            <input
              type="number"
              className="form-control"
              value={this.state.year}
              onChange={this.onChangeYear}
            />
          </div>
          <div className="form-group">
            <label>Color: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.color}
              onChange={this.onChangeColor}
            />
          </div>
          <div className="form-group">
            <label>VIN: </label>
            <input
              type="text"
              className="form-control"
              minLength="17"
              maxLength="17"
              value={this.state.vin}
              onChange={this.onChangeVin}
            />
          </div>

          <div className="form-group">
            <input type="submit" value="Add Car" className="btn btn-primary" />
          </div>
        </form>
      </div>
    )
  }
}