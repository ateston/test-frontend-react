import React, { Component } from 'react';
import axios from 'axios';

const Car = props => (
  <tr>
    <td>{props.car.brand}</td>
    <td>{props.car.model}</td>
    <td>{props.car.year}</td>
    <td>{props.car.color}</td>
    <td>{props.car.vin}</td>
    <td><a href="#" onClick={() => { props.deleteCar(props.car._id) }}>delete</a></td>
  </tr>
);

export default class CarsList extends Component {
  constructor(props) {
    super(props);

    this.deleteCar = this.deleteCar.bind(this);
    this.state = { cars: [] };
  }

  componentDidMount() {
    axios.get('http://localhost:5000/cars/')
      .then(response => {
        this.setState({ cars: response.data })
      })
      .catch((error) => {
        console.log(error)
      })
  }

  deleteCar(id) {
    axios.delete('http://localhost:5000/cars/' + id)
      .then(res => console.log(res.data));

    this.setState({
      cars: this.state.cars.filter(element => element._id !== id)
    })
  }

  carsList() {
    return this.state.cars.map(currentCar => {
      return <Car car={currentCar} deleteCar={this.deleteCar} key={currentCar._id} />;
    });
  }

  render() {
    return (
      <div>
        <h3>Car List</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Brand</th>
              <th>Model</th>
              <th>Year</th>
              <th>Color</th>
              <th>VIN</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.carsList()}
          </tbody>
        </table>
      </div>
    );
  }
}